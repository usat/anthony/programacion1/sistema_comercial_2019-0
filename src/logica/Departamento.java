/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author laboratorio_computo
 */
public class Departamento extends Conexion
{
    private String codigo_departamento,nombre ;
    
    public static ArrayList<Departamento> listarDepartamento= new ArrayList<Departamento>();

    public String getCodigo_departamento() {
        return codigo_departamento;
    }

    public void setCodigo_departamento(String codigo_departamento) {
        this.codigo_departamento = codigo_departamento;
    }    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    private void cargarDatos() throws Exception
    {
        String sql = "select * from departamento order by nombre";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        listarDepartamento.clear();
        while(resultado.next())
        {
            Departamento objMarca = new Departamento();
            objMarca.setCodigo_departamento(resultado.getString("codigo_departamento"));
            objMarca.setNombre(resultado.getString("nombre"));
            listarDepartamento.add(objMarca);
        }
    }
    
    public void llenarComboDepartamento(JComboBox combo) throws Exception
    {
        this.cargarDatos();
        combo.removeAllItems();
        for(int i = 0; i<listarDepartamento.size(); i++)
        {
            Departamento m = listarDepartamento.get(i);
            combo.addItem(m.getNombre());
        }
    }
    
    
    
    
}
