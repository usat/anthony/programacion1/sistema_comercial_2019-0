/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;
import static logica.Categoria.listarCategorias;

/**
 *
 * @author laboratorio_computo
 */
public class Linea extends Conexion
{
    private int codigoLinea;
    private String descripcion;
    public static ArrayList<Linea> listarLineas= new ArrayList<Linea>();

    public int getCodigoLinea() {
        return codigoLinea;
    }

    public void setCodigoLinea(int codigoLinea) {
        this.codigoLinea = codigoLinea;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    private void cargarDatos() throws Exception
    {
        String sql = "select * from linea order by descripcion asc";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        listarLineas.clear();
        while(resultado.next())
        {
            Linea objMarca = new Linea();
            objMarca.setCodigoLinea(resultado.getInt("codigo_linea"));
            objMarca.setDescripcion(resultado.getString("descripcion"));
            listarLineas.add(objMarca);
        }
    }
    
    public void llenarComboLinea(JComboBox combo) throws Exception
    {
        this.cargarDatos();
        combo.removeAllItems();
        for(int i = 0; i<listarLineas.size(); i++)
        {
            Linea m = listarLineas.get(i);
            combo.addItem(m.getDescripcion());
        }
    }
    
    
    
    
}
