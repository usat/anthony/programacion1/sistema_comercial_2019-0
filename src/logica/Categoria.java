/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author laboratorio_computo
 */
public class Categoria extends Conexion
{
    private int codigoCategoria;
    private String descripcion;
    private int codigoLinea;
    
    public static ArrayList<Categoria> listarCategorias = new ArrayList<Categoria>();

    public int getCodigoCategoria() {
        return codigoCategoria;
    }

    public void setCodigoCategoria(int codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigoLinea() {
        return codigoLinea;
    }

    public void setCodigoLinea(int codigoLinea) {
        this.codigoLinea = codigoLinea;
    }
    
    private void cargarDatos(int codigoLinea) throws Exception {
        String sql = "select * from categoria where codigo_linea = ?";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setInt(1, codigoLinea);
        ResultSet resultado = sp.executeQuery();
        listarCategorias.clear();

        while (resultado.next()) {
            Categoria objCategoria = new Categoria();
            objCategoria.setCodigoCategoria(resultado.getInt("codigo_categoria"));
            objCategoria.setCodigoLinea(resultado.getInt("codigo_linea"));
            objCategoria.setDescripcion(resultado.getString("descripcion"));
            listarCategorias.add(objCategoria);
        }
    }
    
    public void llenarComboCategoria(JComboBox combo, int codigoLinea) throws Exception
    {
        this.cargarDatos(codigoLinea);
        combo.removeAllItems();
        for(int i = 0; i<listarCategorias.size(); i++)
        {
            Categoria m = listarCategorias.get(i);
            combo.addItem(m.getDescripcion());
        }
    }
    
}
