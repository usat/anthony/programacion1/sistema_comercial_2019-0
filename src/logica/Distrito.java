package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Distrito extends Conexion
{
    private String codigo_departamento,codigo_provincia,codigo_distrito,nombre;
    
    public static ArrayList<Distrito> listarDistrito = new ArrayList<Distrito>();

    public String getCodigo_departamento() {
        return codigo_departamento;
    }

    public void setCodigo_departamento(String codigo_departamento) {
        this.codigo_departamento = codigo_departamento;
    }

    public String getCodigo_provincia() {
        return codigo_provincia;
    }

    public void setCodigo_provincia(String codigo_provincia) {
        this.codigo_provincia = codigo_provincia;
    }

    public String getCodigo_distrito() {
        return codigo_distrito;
    }

    public void setCodigo_distrito(String codigo_distrito) {
        this.codigo_distrito = codigo_distrito;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    private void cargarDatos(String codigoDepartamento, String codigo_provincia) throws Exception {
        String sql = "Select * from distrito where codigo_departamento =? and codigo_provincia = ?";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setString(1, codigoDepartamento);
        sp.setString(2, codigo_provincia);
        ResultSet resultado = sp.executeQuery();
        listarDistrito.clear();

        while (resultado.next()) {
            Distrito objDistrito = new Distrito();
            objDistrito.setCodigo_departamento(resultado.getString("codigo_departamento"));
            objDistrito.setCodigo_provincia(resultado.getString("codigo_provincia"));
            objDistrito.setCodigo_distrito(resultado.getString("codigo_distrito"));
            objDistrito.setNombre(resultado.getString("nombre"));
            listarDistrito.add(objDistrito);
        }
    }
    
    public void llenarComboDistrito(JComboBox combo, String codigoDepartamento, String codigoProvincia) throws Exception
    {
        this.cargarDatos(codigoDepartamento,codigoProvincia);
        combo.removeAllItems();
        for(int i = 0; i<listarDistrito.size(); i++)
        {
            Distrito m = listarDistrito.get(i);
            combo.addItem(m.getNombre());
        }
    }
    
}
