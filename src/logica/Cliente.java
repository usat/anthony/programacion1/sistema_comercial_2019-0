/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author silviopd
 */
public class Cliente extends Conexion{
    
    public ResultSet listar() throws Exception {
        String sql = "SELECT \n" +
                    "  cliente.codigo_cliente, \n" +
                    "  cliente.apellido_paterno ||' '||cliente.apellido_materno||', '|| cliente.nombres as nombres, \n" +
                    "  cliente.nro_doc_ide, \n" +
                    "  cliente.direccion, \n" +
                    "  cliente.telefono_fijo, \n" +
                    "  cliente.telefono_movil1, \n" +
                    "  cliente.telefono_movil2, \n" +
                    "  cliente.email, \n" +
                    "  departamento.nombre as departamento, \n" +
                    "  provincia.nombre as provincia, \n" +
                    "  distrito.nombre as distrito\n" +
                    "FROM \n" +
                    "  public.cliente, \n" +
                    "  public.departamento, \n" +
                    "  public.provincia, \n" +
                    "  public.distrito\n" +
                    "WHERE \n" +
                    "  provincia.codigo_departamento = departamento.codigo_departamento AND\n" +
                    "  distrito.codigo_departamento = provincia.codigo_departamento AND\n" +
                    "  distrito.codigo_provincia = provincia.codigo_provincia AND\n" +
                    "  distrito.codigo_departamento = cliente.codigo_departamento AND\n" +
                    "  distrito.codigo_provincia = cliente.codigo_provincia AND\n" +
                    "  distrito.codigo_distrito = cliente.codigo_distrito;";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }
}
