/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author laboratorio_computo
 */
public class Provincia extends Conexion
{
    private String codigo_departamento,codigo_provincia,nombre;
    
    public static ArrayList<Provincia> listarProvincia = new ArrayList<Provincia>();

    public String getCodigo_departamento() {
        return codigo_departamento;
    }

    public void setCodigo_departamento(String codigo_departamento) {
        this.codigo_departamento = codigo_departamento;
    }

    public String getCodigo_provincia() {
        return codigo_provincia;
    }

    public void setCodigo_provincia(String codigo_provincia) {
        this.codigo_provincia = codigo_provincia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private void cargarDatos(String codigoDepartamento) throws Exception {
        String sql = "select * from provincia where codigo_departamento = ?";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        sp.setString(1, codigoDepartamento);
        ResultSet resultado = sp.executeQuery();
        listarProvincia.clear();

        while (resultado.next()) {
            Provincia objCategoria = new Provincia();
            objCategoria.setCodigo_departamento(resultado.getString("codigo_departamento"));
            objCategoria.setCodigo_provincia(resultado.getString("codigo_provincia"));
            objCategoria.setNombre(resultado.getString("nombre"));
            listarProvincia.add(objCategoria);
        }
    }
    
    public void llenarComboProvincia(JComboBox combo, String codigoDepartamento) throws Exception
    {
        this.cargarDatos(codigoDepartamento);
        combo.removeAllItems();
        for(int i = 0; i<listarProvincia.size(); i++)
        {
            Provincia m = listarProvincia.get(i);
            combo.addItem(m.getNombre());
        }
    }
    
}
