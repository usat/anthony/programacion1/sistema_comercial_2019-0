/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author laboratorio_computo
 */
public class Articulo extends Conexion {

    /*
      codigo_articulo integer NOT NULL,
  nombre character varying(100) NOT NULL,
  precio_venta numeric(14,2) NOT NULL,
  codigo_categoria integer NOT NULL,
  codigo_marca integer,
  stock integer DEFAULT 0,
  tipo_articulo character(1),
  tiene_descuento character(1),
  porcentaje_descuento numeric(5,2),
     */
    private int codigoArticulo;
    private String nombre;
    private double precioVenta;
    private int codigoCategoria;
    private int codigoMarca;
    private int stock;
    private String tipoArticulo;
    private String tieneDescuento;
    private double porcentajeDescuento;

    public int getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(int codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getCodigoCategoria() {
        return codigoCategoria;
    }

    public void setCodigoCategoria(int codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    public int getCodigoMarca() {
        return codigoMarca;
    }

    public void setCodigoMarca(int codigoMarca) {
        this.codigoMarca = codigoMarca;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getTieneDescuento() {
        return tieneDescuento;
    }

    public void setTieneDescuento(String tieneDescuento) {
        this.tieneDescuento = tieneDescuento;
    }

    public double getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(double porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    public ResultSet listar() throws Exception {
        String sql = "select * from fn_articulo_listar()";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "select * from fn_articulo_agregar(?,?,?,?,?,?,?,?)";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getNombre());
            spInsertar.setDouble(2, this.getPrecioVenta());
            spInsertar.setInt(3, this.getCodigoCategoria());
            spInsertar.setInt(4, this.getCodigoMarca());
            spInsertar.setInt(5, this.getStock());
            spInsertar.setString(6, this.getTipoArticulo());
            spInsertar.setString(7, this.getTieneDescuento());
            spInsertar.setDouble(8, this.getPorcentajeDescuento());
            ResultSet resultado = this.ejecutarSQL2(spInsertar);

            if (resultado.next()) {
                transaccion.commit();
            } else {
                transaccion.rollback();
            }

            transaccion.close();
            this.cerrarConexion(transaccion);

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public ResultSet leerDatosCodigo(int cod_cat) throws Exception {
        String sql = "SELECT \n"
                + "  articulo.codigo_articulo, \n"
                + "  articulo.nombre, \n"
                + "  articulo.precio_venta, \n"
                + "  linea.codigo_linea, \n"
                + "  linea.descripcion as linea, \n"
                + "  categoria.codigo_categoria, \n"
                + "  categoria.descripcion as categoria, \n"
                + "  marca.codigo_marca, \n"
                + "  marca.descripcion as marca, \n"
                + "  articulo.stock, \n"
                + "  articulo.tipo_articulo, \n"
                + "  articulo.tiene_descuento, \n"
                + "  articulo.porcentaje_descuento\n"
                + "FROM \n"
                + "  public.articulo, \n"
                + "  public.categoria, \n"
                + "  public.linea, \n"
                + "  public.marca\n"
                + "WHERE \n"
                + "  categoria.codigo_linea = linea.codigo_linea AND\n"
                + "  categoria.codigo_categoria = articulo.codigo_categoria AND\n"
                + "  marca.codigo_marca = articulo.codigo_marca\n"
                + "  AND articulo.codigo_articulo = ?;";
        PreparedStatement sentencia = abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, cod_cat);
        return ejecutarSQL(sentencia);
    }

    public boolean editar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "select * from fn_articulo_editar(?,?,?,?,?,?,?,?,?)";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, this.getCodigoArticulo());
            spInsertar.setString(2, this.getNombre());
            spInsertar.setDouble(3, this.getPrecioVenta());
            spInsertar.setInt(4, this.getCodigoCategoria());
            spInsertar.setInt(5, this.getCodigoMarca());
            spInsertar.setInt(6, this.getStock());
            spInsertar.setString(7, this.getTipoArticulo());
            spInsertar.setString(8, this.getTieneDescuento());
            spInsertar.setDouble(9, this.getPorcentajeDescuento());
            ResultSet resultado = this.ejecutarSQL2(spInsertar);

            if (resultado.next()) {
                transaccion.commit();
            } else {
                transaccion.rollback();
            }

            transaccion.close();
            this.cerrarConexion(transaccion);

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public boolean eliminar() throws Exception {
        Connection transaccion = abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "delete from articulo where codigo_articulo=?";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, getCodigoArticulo());

        this.ejecutarSQL(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        this.cerrarConexion(transaccion);

        return true;
    }

}
